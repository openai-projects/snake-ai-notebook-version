import numpy as np

from helper_files.game import SnakeGame
from helper_files.agent import *
from helper_files.plot_helper import *

q_table = np.load("q_table.npy")
agent = Snake_Agent(q_table)

# Values for plotting
total_score = 0
plot_scores= []
plot_mean_scores = []
record_score = 0

for episode in range(1, 16):
    # For every episode reset the game
    game = SnakeGame()
    game.show_ui = True
    game.speed = 50

    # Variables to track the AIs progress
    score = 0
    cum_reward = 0
    episode_steps = 0
    steps_where_reward_does_not_change = 0

    game_over = False

    while not game_over:
        state = agent.get_state(game)        
        action = np.argmax(q_table[state])
        game_over, score, reward = game.play_step(action)
        
    record_score = score if record_score < score else record_score
    plot_scores.append(score)
    total_score += score
    mean_score = total_score / episode
    plot_mean_scores.append(mean_score)
    plot(plot_scores, plot_mean_scores)