from helper_files.agent import *
from helper_files.game import *
from helper_files.state_helper import *
from helper_files.plot_helper import *
from helper_files.action_helper import *

# Create q_table
q_table = np.zeros((STATE_COUNT, len(ACTIONS)))

# Create snake agent
agent = Snake_Agent(q_table)

# Values for plotting
total_score = 0
plot_scores= []
plot_mean_scores = []
record_score = 0

try:
    
    for episode in range(1, EPISODES+1):
        # For every episode reset the game
        game = SnakeGame()
        game.show_ui = True

        # Variables to track the AIs progress
        score = 0
        game_over = False

        while not game_over:
            state = agent.get_state(game)
            
            if random.uniform(0, 1) < RANDOM_FACTOR:
                action = game.get_random_action()
            else:
                action = np.argmax(q_table[state])

            game_over, score, reward = game.play_step(action)
            next_state = agent.get_state(game)

            old_q = q_table[state, action]
            best_action = np.argmax(q_table[next_state])
            q_of_best_action = q_table[next_state, best_action]

            new_q = old_q + ( LEARNING_RATE * ( reward + ( DISCOUNT_FACTOR * ( q_of_best_action - old_q ))))
            q_table[state, action] = new_q
        
        # Add data to plotter
        record_score = score if record_score < score else record_score
        plot_scores.append(score)
        total_score += score
        mean_score = total_score / episode
        plot_mean_scores.append(mean_score)
        plot(plot_scores, plot_mean_scores)
finally:
    np.save("q_table", q_table)