import pygame
import random
import numpy as np
import math

from enum import Enum
from collections import namedtuple
from helper_files.constants import *

pygame.init()
font = pygame.font.Font('arial.ttf', 25)

class Direction(Enum):
    RIGHT = 1
    LEFT = 2
    UP = 3
    DOWN = 4
    
Point = namedtuple('Point', 'x, y')

# rgb colors
WHITE = (255, 255, 255)
RED = (200,0,0)
BLUE1 = (0, 0, 255)
BLUE2 = (0, 100, 255)
BLACK = (0,0,0)

class SnakeGame:
    
    def __init__(self, w=640, h=480):
        self.show_ui = False
        self.speed = SPEED
        self.w = w
        self.h = h
        # init display
        self.display = pygame.display.set_mode((self.w, self.h))
        pygame.display.set_caption('Snake')
        self.clock = pygame.time.Clock()
        
        # init game state
        self.direction = Direction.RIGHT
        
        self.head = Point(self.w/2, self.h/2)
        self.snake = [self.head, 
                      Point(self.head.x-BLOCK_SIZE, self.head.y),
                      Point(self.head.x-(2*BLOCK_SIZE), self.head.y)]
        
        self.score = 0
        self.food = None
        self._place_food()
        
    def _place_food(self):
        x = random.randint(0, (self.w-BLOCK_SIZE )//BLOCK_SIZE )*BLOCK_SIZE 
        y = random.randint(0, (self.h-BLOCK_SIZE )//BLOCK_SIZE )*BLOCK_SIZE
        self.food = Point(x, y)
        if self.food in self.snake:
            self._place_food()
        
    def play_step(self, action_id):
        action = [0, 0, 0]
        action[action_id] = 1
        # 1. collect user input
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
        
        # 2. move
        distance_to_apple = self._calculate_distance_to_food(self.head, self.food)
        self._move(action) # update the head
        distance_to_apple_after_moving = self._calculate_distance_to_food(self.head, self.food)
        self.snake.insert(0, self.head)
        
        # 3. check if game over
        game_over = False
        reward = 0
        
        if distance_to_apple_after_moving < distance_to_apple:
            reward += 5
        else:
            reward -= 5
        
        if self.is_collision():
            game_over = True
            reward -= 10
            return game_over, self.score, reward
            
        # 4. place new food or just move
        if self.head == self.food:
            self.score += 1
            reward += 10
            self._place_food()
        else:
            self.snake.pop()
        
        # 5. update ui and clock
        if self.show_ui:
            self._update_ui()
        self.clock.tick(self.speed)
        # 6. return game over and score
        return game_over, self.score, reward
    
    def is_collision(self, head=None):
        # hits boundary
        if head is None:
            head = self.head
        if head.x > self.w - BLOCK_SIZE or head.x < 0 or head.y > self.h - BLOCK_SIZE or head.y < 0:
            return True
        # hits itself
        if head in self.snake[1:]:
            return True
        
        return False
        
    def _update_ui(self):
        self.display.fill(BLACK)
        
        for pt in self.snake:
            pygame.draw.rect(self.display, BLUE1, pygame.Rect(pt.x, pt.y, BLOCK_SIZE, BLOCK_SIZE))
            pygame.draw.rect(self.display, BLUE2, pygame.Rect(pt.x+4, pt.y+4, 12, 12))
            
        pygame.draw.rect(self.display, RED, pygame.Rect(self.food.x, self.food.y, BLOCK_SIZE, BLOCK_SIZE))
        
        text = font.render("Score: " + str(self.score), True, WHITE)
        self.display.blit(text, [0, 0])
        pygame.display.flip()
        
    def _move(self, action):
        # [straight, right, left] ex: [1, 0, 0]
        x = self.head.x
        y = self.head.y
        
        clockwise = [Direction.UP, Direction.RIGHT, Direction.DOWN, Direction.LEFT]
        direction_index = clockwise.index(self.direction)
        
        if np.array_equal(action, [1, 0, 0]): # Straight
            self.direction = clockwise[direction_index] # Nothing happens, direction stays the same
        elif np.array_equal(action, [0, 1, 0]): # Right
            self.direction = clockwise[(direction_index + 1) % 4] # Direction going 1 up clockwise
        else: # Left
            self.direction = clockwise[(direction_index - 1) % 4] # Direction going 1 up counter-clockwise
        
        if self.direction == Direction.RIGHT:
            x += BLOCK_SIZE
        elif self.direction == Direction.LEFT:
            x -= BLOCK_SIZE
        elif self.direction == Direction.DOWN:
            y += BLOCK_SIZE
        elif self.direction == Direction.UP:
            y -= BLOCK_SIZE
            
        self.head = Point(x, y)
    
    def _calculate_distance_to_food(self, head, food):
        horizontal_distance = np.abs(head.x - food.x)
        vertical_distance = np.abs(head.y - food.y)
        
        return math.sqrt(horizontal_distance ** 2 + vertical_distance ** 2)
    
    def get_random_action(self):
        action = [0, 0, 0]
        action[2] = 1

        return 1
        
