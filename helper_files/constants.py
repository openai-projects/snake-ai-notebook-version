EPISODES = 1_000

OBSERVATIONS = [
    "Danger Right",     # Either 1 or 0, multiple danger indicators can be active at one time
    "Danger Left",      # 2 to the power of 3 possible values
    "Danger Straight",
    
    "Direction Up",     # Either 1 or 0, only one direction can be active at one time
    "Direction Down",   # 4 different values 
    "Direction Left",
    "Direction Right",
    
    "Apple Up",         # Either 1 or 0, only 2 directions can be active at most at one time
    "Apple Down",       # 8 possible values: left, right, up, down, leftup, leftdown, rightup & rightdown
    "Apple Left",
    "Apple Right"
]

# Example: [1, 0, 0] means that action is straight
ACTIONS = [
    "Straight",
    "Right",
    "Left"
]

STATE_COUNT = (2 * 2 * 2) * 4 * 8

LEARNING_RATE = 0.1
DISCOUNT_FACTOR = 0.1
RANDOM_FACTOR = 0.1

BLOCK_SIZE = 20
SPEED = 100_000
