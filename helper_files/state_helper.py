apple_values = [
    (0, 1),
    (0, 2),
    (1, 0),
    (1, 1),
    (1, 2),
    (2, 0),
    (2, 1),
    (2, 2)
]

possible_state_values = []

for x in range(8):
    for y in range(1, 5):
        for z in range(len(apple_values)):
            possible_state_values.append((x, y, apple_values[z]))

def get_state_id(state):
    first = "".join(map(str, state[:3]))
    first = int(first, 2)

    second = "".join(map(str, state[3:7]))
    second = 4 - int(second.index('1'))

    third = "".join(map(str, state[7:]))
    third = (int(third[::-1][:2], 2), int(third[::-1][2:], 2))

    state_value = (first, second, third)
    state_id = possible_state_values.index(state_value)
    
    return state_id
