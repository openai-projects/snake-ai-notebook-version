import random
import numpy as np
from helper_files.constants import *
from helper_files.game import BLOCK_SIZE, Point, Direction, SnakeGame
from helper_files.state_helper import *

class Snake_Agent:
    def __init__(self, q_table):
        self.q_table = q_table
        self.n_games = 0        
    
    def get_state(self, game):
        snake_head = game.snake[0]
        food = game.food

        point_l = Point(snake_head.x - BLOCK_SIZE, snake_head.y)
        point_r = Point(snake_head.x + BLOCK_SIZE, snake_head.y)
        point_u = Point(snake_head.x, snake_head.y + BLOCK_SIZE)
        point_d = Point(snake_head.x, snake_head.y - BLOCK_SIZE)

        dir_l = game.direction == Direction.LEFT
        dir_r = game.direction == Direction.RIGHT
        dir_u = game.direction == Direction.UP
        dir_d = game.direction == Direction.DOWN

        state = [                        
            # Danger right
            (dir_l and game.is_collision(point_u)) or
            (dir_r and game.is_collision(point_d)) or
            (dir_u and game.is_collision(point_r)) or
            (dir_d and game.is_collision(point_l)),
            
            # Danger left
            (dir_l and game.is_collision(point_d)) or
            (dir_r and game.is_collision(point_u)) or
            (dir_u and game.is_collision(point_l)) or
            (dir_d and game.is_collision(point_r)),
            
            # Danger straight
            (dir_l and game.is_collision(point_l)) or
            (dir_r and game.is_collision(point_r)) or
            (dir_u and game.is_collision(point_u)) or
            (dir_d and game.is_collision(point_d)),
            
            dir_u,
            dir_d,
            dir_l,
            dir_r,
            
            food.y > snake_head.y, # Apple up
            food.y < snake_head.y, # Apple down
            food.x < snake_head.x, # Apple left
            food.x > snake_head.x  # Apple right            
        ]
        
        return get_state_id(np.array(state, dtype=int))
 