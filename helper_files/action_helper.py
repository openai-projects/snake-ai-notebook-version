import numpy as np

def get_action_id(action):
    return np.argmax(action)